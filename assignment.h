#include <iostream>
#include <string>
#include <set>
#include <set>
#include <algorithm>
#include <assert.h>


struct Data {
    uint8_t x;
    double y;
    
    Data(uint8_t a, double b) {
        x = a;
        y = b;
    }
    bool friend operator<(const Data& a, const Data& b)
    {
        return a.y < b.y;
        
    }
    
};

void callback_func(Data&);
std::set<Data> smallestAscendingY(std::set<Data>&, int);
std::set<Data> reOrderTheContainer(std::set<Data>&, std::set<Data>&);
void categorizeTheSet(std::set<Data>&, uint8_t, std::set<Data>&, std::set<Data>&);
void printData(std::set<Data>&);
typedef void (*ptr)(Data&);
void assignmentFunction(std::set<Data>&, uint8_t, ptr, int);