#include <iostream>
#include <string>
#include <set>
#include <set>
#include <algorithm>
#include <cassert>
#include <assert.h>
#include "assignment.h"
#include <stdio.h>
using namespace std;
#ifdef NDEBUG
#  define assert(condition) ((void)0)
#endif
//Assignment 1: Reorders the container in such a way, that subset A preceeds subset B.
void testReorder()
{
    std::set<Data> subSetA;
    std::set<Data> subSetB;
    std::set<Data> container;
    std::uint8_t x_threshold = 5;
    container.insert({ 1,1.1 });
    container.insert({ 3,3.3 });
    container.insert({ 6,6.7 });
    container.insert({ 8,8.8 });
    container.insert({ 9,9.8 });
    categorizeTheSet(container, x_threshold, subSetA, subSetB);
    std::set<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
    assert(orderedContainer.size() == 5);
    //subSetA size should be 3
    assert(subSetA.size() == 3);
    //subSetB size should be 2
    assert(subSetB.size() == 2);
    //subSetA`s First Element should be {6, 6.7}
    std::set<Data>::iterator it = subSetA.begin();
    assert(it->x == 6 && it->y == 6.7);
    std::advance(it, 1);
    assert(it->x == 8 && it->y == 8.8);
    std::advance(it, 1);
    assert(it->x == 9 && it->y == 9.8);
    std::set<Data>::iterator itB = subSetB.begin();
    assert(itB->x == 1 && itB->y == 1.1);
    std::advance(itB, 1);
    assert(itB->x == 3 && itB->y == 3.3);
}
//Assignment 2: Reorders elements of subset B in such a way,
//that given number n of elements has smallest value of y and this n elements are
//in ascending order of y. Let this n elements be subset C.

void testAssending()
{
    std::set<Data> subSetA;
    std::set<Data> subSetB;
    std::set<Data> container;
    std::uint8_t x_threshold = 5;
    container.insert({ 1,1.1 });
    container.insert({ 3,3.3 });
    container.insert({ 6,6.7 });
    container.insert({ 8,8.8 });
    container.insert({ 9,9.8 });
    categorizeTheSet(container, x_threshold, subSetA, subSetB);
    std::set<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
    std::set<Data> c = smallestAscendingY(subSetB, 2);

    //First smallestAscending
    std::set<Data>::iterator itB = subSetB.begin();
    assert(itB->x == 1 && itB->y == 1.1);
    //Second smallestAscending
    std::advance(itB, 1);
    assert(itB->x == 3 && itB->y == 3.3);
}
//Assignment 3: Call the callback function and pass the subset C
void testCallBack()
{
    ptr callback = callback_func;
    std::set<Data> container;
    container.insert({ 1,1.1 });
    //Data d = container.find(0);
    //callback(d);

      
}
void negativeTestReorder()
{
    std::set<Data> subSetA;
    std::set<Data> subSetB;
    std::set<Data> container;
    std::uint8_t x_threshold = 5;
    container.insert({ 1,1.1 });
    container.insert({ 3,3.3 });
    container.insert({ 6,6.7 });
    container.insert({ 8,8.8 });
    container.insert({ 9,9.8 });
    categorizeTheSet(container, x_threshold, subSetA, subSetB);
    std::set<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
    assert(orderedContainer.size() != 5);
    //subSetA size should be 3
    assert(subSetA.size() != 3);
    //subSetB size should be 2
    assert(subSetB.size() != 2);
    //subSetA`s First Element should be {6, 6.7}
    std::set<Data>::iterator it = subSetA.begin();
    assert(it->x != 6 && it->y != 6.7);
}
void negativeTestAssending()
{
    std::set<Data> subSetA;
    std::set<Data> subSetB;
    std::set<Data> container;
    std::uint8_t x_threshold = 5;
    container.insert({ 1,1.1 });
    container.insert({ 3,3.3 });
    container.insert({ 6,6.7 });
    container.insert({ 8,8.8 });
    container.insert({ 9,9.8 });
    categorizeTheSet(container, x_threshold, subSetA, subSetB);
    std::set<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
    std::set<Data> c = smallestAscendingY(subSetB, 2);
    //First smallestAscending
    std::set<Data>::iterator itB = subSetB.begin();
    assert(itB->x != 1 && itB->y != 1.1);
    //Second smallestAscending
    std::advance(itB, 1);
    assert(itB->x != 3 && itB->y != 3.3);
}
int main()
{
    //Test the reorder functionallty 
    testReorder();
    testAssending();
    testCallBack();
    negativeTestReorder();
    negativeTestAssending();

}