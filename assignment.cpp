
#include <iostream>
#include <string>
#include <set>
#include <set>
#include <algorithm>
#include <numeric>
#include "assignment.h"
using namespace std;

// Library method for callback function, that returns nothing
void callback_func(Data& inputStruct)
{
    cout << "\n callback function";
}

// Library method to order subset A and then subset B, and return to the caller.
set<Data> smallestAscendingY(set<Data>& subSetA, int n) {
    std::set<Data> myset;
    std::set<Data>::iterator it = subSetA.begin();
    for (int i = 0; i < n; i++)
    {
        myset.insert(*it);
        std::advance(it, 1);
        
    }
     return myset;
 }

// Library method to order subset A and then subset B, and return to the caller.
set<Data> reOrderTheContainer(set<Data>& subSetA, set<Data>& subSetB) {
    std::set<Data> AB = subSetA;
    merge(subSetA.begin(), subSetA.end(), subSetB.begin(), subSetB.end(), inserter(AB,AB.begin()));
    return AB;
}

void categorizeTheSet(set<Data>& container, uint8_t x_threshold, set<Data>& subSetA, set<Data>& subSetB) {
    for (Data entry : container) {
        if ((unsigned)entry.x > (unsigned)x_threshold) {
            subSetA.insert(entry);
        }
        else {
            subSetB.insert(entry);
        }
    }
}
void printData(set<Data>& container) {
    for (Data d : container) {
        cout << "\n x: " << d.x << ", y: " << d.y;
    }
}
// This is the entry point of library
/*Implement library with function which :
1. Reorders the container in such a way, that subset A preceeds subset B.
2. Reorders elements of subset B in such a way, that given number ```n``` of elements has smallest value of y and this n elements are in ascending order of y.Let this ```n``` elements be subset C.
3. Accepts callback as a parameter and applies it to elements of subset C.Callback takes ```Data&``` as parameter and returns no result.*/

void assignmentFunction(set<Data>& container, uint8_t x_threshold, ptr callback, int n) {
    std::set<Data> subSetA;
    std::set<Data> subSetB;
    categorizeTheSet(container, x_threshold, subSetA, subSetB);
    std::set<Data> orderedContainer = reOrderTheContainer(subSetA, subSetB);
    std::set<Data> c = smallestAscendingY(subSetB, n);
    for (Data dataOfC : c) {
        callback(dataOfC);
    }
}



